<%@ page import="java.util.List" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>SeeLiveTV</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="robots" content="index, follow">

	<!-- icons -->
	<link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png">
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Bootstrap Core CSS file -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">

	<!-- Override CSS file - add your own CSS rules -->
	<link rel="stylesheet" href="assets/css/styles.css">
</head>
<body>
	<center>
		<img src="/images/seebo_feature_graphic.png" align="bottom" height="250" width="500">
		<br><br>
		<h2> Welcome to SeeLiveTV Channel Manager DashBoard</h2>
	</center>
	<br>
	<br>
	<center>
	 	<form name="SeeListForm" action="/list" method="post">
           <select name="category" id="dropdown">
                <option value="dropdown">Please select a category</option>
                <option value="eco">ECO TV</option>
                <option value="chinese">Chinese TV</option>
                <option value="greek">Greek TV</option>
            </select>
            <input type="submit" value="Show channels">
        </form>
	</center>
</body>
</html>

