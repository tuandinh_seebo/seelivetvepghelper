package au.com.seebo.android.seeboeco.backend.seelivetvspring.models;
import java.util.List;

/**
 * Created by root on 6/16/15.
 */
public class LiveTVChannel {

    private String name;
    private String streamingUrl;
    private List<Programme> epgData;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreamingUrl() {
        return streamingUrl;
    }

    public void setStreamingUrl(String streamingUrl) {
        this.streamingUrl = streamingUrl;
    }

    public List<Programme> getEpgData() {
        return epgData;
    }

    public void setEpgData(List<Programme> epgData) {
        this.epgData = epgData;
    }


    public String toString() {

        return "Channel Name : " + getName() + " Channel Url : " + getStreamingUrl();

    }

    public boolean isValidObj() {

        if (getName() != null && getStreamingUrl() != null) {
            return true;
        }

        return false;

    }

    public Programme getCurrentProgramme() {

        Programme matchedProgramme = null;

        for (Programme programme : epgData) {

            String start = programme.getStart();
            String end = programme.getEnd();

            long actualStartTime = Long.parseLong(start.trim());
            long actualEndTime = Long.parseLong(end.trim());
            long currentTime = System.currentTimeMillis()/1000;

            if ((actualStartTime <= currentTime) && (currentTime <= actualEndTime)) {
                matchedProgramme = programme;
                break;
            }

        }

        return matchedProgramme;

    }

    public Programme getNextProgramme() {

        Programme matchedProgramme = null;
        boolean isAfterCurrent = false;

        for (Programme programme : epgData) {

            if (isAfterCurrent) {
                matchedProgramme = programme;
                break;
            }

            String start = programme.getStart();
            String end = programme.getEnd();

            long actualStartTime = Long.parseLong((start.trim()));
            long actualEndTime = Long.parseLong(end.trim());
            long currentTime = System.currentTimeMillis()/1000;

            if ((actualStartTime <= currentTime) && (currentTime <= actualEndTime)) {
                isAfterCurrent = true;
            }


        }

        return matchedProgramme;

    }

}
