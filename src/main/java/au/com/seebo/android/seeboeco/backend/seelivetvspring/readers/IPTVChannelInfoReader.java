package au.com.seebo.android.seeboeco.backend.seelivetvspring.readers;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import au.com.seebo.android.seeboeco.backend.seelivetvspring.builders.ResultBuilder;
import au.com.seebo.android.seeboeco.backend.seelivetvspring.models.IPTVChannel;
import au.com.seebo.android.seeboeco.backend.seelivetvspring.models.Programme;
import au.com.seebo.android.seeboeco.backend.seelivetvspring.models.ReaderErrorCodes;
import au.com.seebo.android.seeboeco.backend.seelivetvspring.models.Result;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created by root on 6/18/15.
 */
public class IPTVChannelInfoReader {

    private String channelInfoUtl;
    private String epgDataUrl;
    final String CHANNEL_SPLIT_REG_EX = "#EXTINF";
    final String CHANNEL_DATA_SPLIT_REG_EX = "\n";

    private Document document;
    
    public IPTVChannelInfoReader(String infoUrl,String epgUrl){
    	channelInfoUtl = infoUrl;
    	epgDataUrl = epgUrl;
    }

    public Result read() throws Throwable {

        Result infoFileFetchResult = fetchIPTVFile();

        if(infoFileFetchResult.getType().equals("ERROR")){
            return infoFileFetchResult;
        }

        Result epgFileFetchResult = fetchIPTVEPGFile();

        if(epgFileFetchResult.getType().equals("ERROR")){
            return epgFileFetchResult;
        }

        Result epgFileParseResult = parseEPGContentToXML(String.valueOf(epgFileFetchResult.getData()));

        if(epgFileParseResult.getType().equals("ERROR")){
            return epgFileParseResult;
        }

        String fileContent = preProcessContent(String.valueOf(infoFileFetchResult.getData()));

        return ResultBuilder.buildSuccessMsg("Reading IPTV Info Success", extractChannelInfo(fileContent));

    }

    private Result fetchIPTVFile(){


        CloseableHttpClient client = HttpClients.custom().build();

        CloseableHttpResponse response = null;

        HttpGet req = new HttpGet(channelInfoUtl);

        try {

            response = client.execute(req);
            HttpEntity entity = response.getEntity();

            return ResultBuilder.buildSuccessMsg("IPTV Channel Info Fetched Successfully", EntityUtils.toString(entity));

        } catch (FileNotFoundException e) {
            return ResultBuilder.buildErrorMsg("Failed to Fetch IPTV Channel Info. Reason : " + e.getMessage(), null, ReaderErrorCodes.FILE_NOT_FOUND);
        }catch (Exception e) {
            return ResultBuilder.buildErrorMsg("Failed to Fetch IPTV Channel Info. Reason : " + e.getMessage(), null, ReaderErrorCodes.GENERAL_HTTP_ERROR);
        }finally {
            try {
                response.close();
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private Result fetchIPTVEPGFile(){

        CloseableHttpClient client = HttpClients.custom().build();

        CloseableHttpResponse response = null;

        HttpGet req = new HttpGet(epgDataUrl);

        try {

            response = client.execute(req);
            HttpEntity entity = response.getEntity();

            return ResultBuilder.buildSuccessMsg("IPTV EPG Data file Fetched Successfully", EntityUtils.toString(entity));

        } catch (FileNotFoundException e) {
            return ResultBuilder.buildErrorMsg("Failed to Fetch IPTV EPG Data file. Reason : " + e.getMessage(), null, ReaderErrorCodes.FILE_NOT_FOUND);
        }catch (Exception e) {
            return ResultBuilder.buildErrorMsg("Failed to Fetch IPTV EPG Data file. Reason : " + e.getMessage(), null, ReaderErrorCodes.GENERAL_HTTP_ERROR);
        }finally {
            try {
                response.close();
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private List<IPTVChannel> extractChannelInfo(String content) throws Throwable {

        List<IPTVChannel> channelList = new LinkedList<IPTVChannel>();

        Pattern channelSplitPattern = Pattern.compile(CHANNEL_SPLIT_REG_EX);

        String[]channelInfoSections = channelSplitPattern.split(content);

        for (int i = 0 ; i < channelInfoSections.length; i++){

            String channelInfoSection = channelInfoSections[i];
            Pattern channelDataLineSplitPattern = Pattern.compile(CHANNEL_DATA_SPLIT_REG_EX);

            String[] channelDataLines = channelDataLineSplitPattern.split(channelInfoSection);

            IPTVChannel channel = new IPTVChannel();

            for (int j = 0 ; j < channelDataLines.length; j++){

                String channelDataLine = channelDataLines[j];

                if (!channelDataLine.startsWith("#")){
                    // if tvg name, tvg logo attributes there -> this is channel info line, Otherwise it is channel url part
                    if (channelDataLine.indexOf("tvg-name") > -1 && channelDataLine.indexOf("tvg-logo") > -1){
                        channel.setTvgName(extractChannelAttribute(channelDataLine,"tvg-name"));
                        channel.setLogoName(extractChannelAttribute(channelDataLine,"tvg-logo"));
                        channel.setName(nameCleanUp(extractChannelName(channelDataLine)));
                    }else{
                        channel.setStreamingUrl(channelDataLine);
                    }

                }

            }

            if (channel.getName() != null && channel.getTvgName() != null && channel.getLogoName() != null && channel.getStreamingUrl() != null){
            	channel.setEpgData(getEPGData(channel.getTvgName()));
                channelList.add(channel);
            }

        }

        return channelList;
    }

    private String extractChannelAttribute(String dataLine,String tagName){


        Pattern channelAttrPattern = Pattern.compile(tagName + "=\"(.*)\"");
        Pattern attrValuePattern = Pattern.compile("\"([^\"]*)\"");
        Matcher channelAttrMatcher = channelAttrPattern.matcher(dataLine);

        String attrVal = null;

        if (channelAttrMatcher.find()){

            Matcher attrValueMatcher = attrValuePattern.matcher(channelAttrMatcher.group(0));
            if (attrValueMatcher.find()){
                attrVal = attrValueMatcher.group(1);
            }
        }

        return attrVal;

    }

    private String extractChannelName(String dataLine){


        String channelName = null;

        if (dataLine.indexOf(",") > -1){

            String[] dataParts = dataLine.split(",");

            if (dataParts.length > 0){
                channelName = dataParts[dataParts.length-1];
            }

        }

        return channelName;

    }

    private String preProcessContent(String content){

        return content.replace("#EXTM3U","");

    }

    /*
    *
    *   EPG Related functions
    *
    * */


    private Result parseEPGContentToXML(String xmlContent){

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {

            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(new InputSource(new StringReader(xmlContent)));

            return ResultBuilder.buildSuccessMsg("XML data parsed Successfully", null);

        } catch (ParserConfigurationException e) {
            return ResultBuilder.buildErrorMsg("Failed to parse XML data. Reason : " + e.getMessage(), null,ReaderErrorCodes.IP_TV_EPG_PARSE_ERROR);
        } catch (SAXException e) {
            return ResultBuilder.buildErrorMsg("Failed to parse XML data. Reason : " + e.getMessage(), null,ReaderErrorCodes.IP_TV_EPG_PARSE_ERROR);
        } catch (IOException e) {
            return ResultBuilder.buildErrorMsg("Failed to parse XML data. Reason : " + e.getMessage(), null,ReaderErrorCodes.IP_TV_EPG_PARSE_ERROR);
        }

    }

    private List<Programme> getEPGData(String tvgName){

        String programmeNodeExp = "//*[@channel='" + tvgName + "']";

        XPathFactory factory = XPathFactory.newInstance();
        XPath xPath = factory.newXPath();

        List<Programme> epgData = new LinkedList<Programme>();

        try {

            NodeList nodeList = (NodeList) xPath.evaluate(programmeNodeExp,document, XPathConstants.NODESET);

            for(int i = 0 ; i < nodeList.getLength() ; i++){

                Programme programme = new Programme();

                Element element = (Element) nodeList.item(i);
                programme.setStart(element.getAttribute("start"));
                programme.setEnd(element.getAttribute("stop"));

                NodeList titleNodeList = element.getElementsByTagName("title");

                if (titleNodeList != null && titleNodeList.getLength() != 0){

                    // assume that there must be one title tag within programme element
                    programme.setTitle(titleNodeList.item(0).getTextContent());
                }

                NodeList descNodeList = element.getElementsByTagName("desc");

                if (descNodeList != null && descNodeList.getLength() != 0){

                    // assume that there must be one desc tag within programme element
                    programme.setDescription(descNodeList.item(0).getTextContent());
                }

                epgData.add(programme);

            }

        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        return epgData;

    }
    
    private String nameCleanUp(String channelName){
    	
    	channelName = channelName.replaceAll("\r", "");
    	Pattern p=Pattern.compile("\\[COLOR.*\\](.*?)\\[\\/COLOR\\]");
        Matcher match=p.matcher(channelName);
        if(match.matches())
        {
            return match.group(1);
        }
    	
    	return channelName;
    }

}
