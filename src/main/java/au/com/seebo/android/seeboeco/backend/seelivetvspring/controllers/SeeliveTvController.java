package au.com.seebo.android.seeboeco.backend.seelivetvspring.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.com.seebo.android.seeboeco.backend.seelivetvspring.dto.MobileDto;
import au.com.seebo.android.seeboeco.backend.seelivetvspring.models.IPTVChannel;
import au.com.seebo.android.seeboeco.backend.seelivetvspring.models.Result;
import au.com.seebo.android.seeboeco.backend.seelivetvspring.readers.IPTVChannelInfoReader;

@RestController
public class SeeliveTvController {

	@SuppressWarnings("unchecked")
	@RequestMapping(value="iptv/default", method=RequestMethod.GET) 
    public MobileDto signupUser() throws Throwable {
        
        MobileDto mDto = new  MobileDto();

        IPTVChannelInfoReader seeboTvReader = new IPTVChannelInfoReader("http://iptv.seebo.com.au/IPTV/playlist.m3u", "http://iptv.seebo.com.au/IPTV/epg.xml"); 
        
        Result seeboTvReaderResult = seeboTvReader.read();

		if(!seeboTvReaderResult.getType().equals("ERROR")){
		    mDto.setSeeboTvChannels((List<IPTVChannel>) seeboTvReaderResult.getData());
		}else{
		        mDto.setSeeboTvErrorInfo(seeboTvReaderResult.getMsg());
		}
		
		IPTVChannelInfoReader greekTvReader = new IPTVChannelInfoReader("http://iptv.seebo.com.au/IPTV/playlist-Greek.m3u", "http://iptv.seebo.com.au/IPTV/epg-Greek.xml");
		        
		        Result greekTvReaderResult = greekTvReader.read();
		
		if(!greekTvReaderResult.getType().equals("ERROR")){
		    mDto.setGreekTvChannels((List<IPTVChannel>) greekTvReaderResult.getData());
		}else{
		        mDto.setGreekTvErrorInfo(greekTvReaderResult.getMsg());
		}
		
		IPTVChannelInfoReader ecoTvReader = new IPTVChannelInfoReader("http://iptv.seebo.com.au/IPTV/playlist-eco.m3u", "http://iptv.seebo.com.au/IPTV/epg-eco.xml");
		        
		        Result ecoTvReaderResult = ecoTvReader.read();
		
		if(!ecoTvReaderResult.getType().equals("ERROR")){
		    mDto.setEcoTvChannels((List<IPTVChannel>) ecoTvReaderResult.getData());
		}else{
		        mDto.setEcoTvErrorInfo(ecoTvReaderResult.getMsg());
		}
        return mDto;
	}
}
