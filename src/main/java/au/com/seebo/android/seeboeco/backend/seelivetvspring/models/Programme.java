package au.com.seebo.android.seeboeco.backend.seelivetvspring.models;

/**
 * Created by root on 6/8/15.
 */
public class Programme {

    private String start;
    private String end;
    private String title;
    private String description;

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString(){

        return "Start : " + getStart() + " End  : " + getEnd() + " Title : " + getTitle() + " Description : " + getDescription();

    }

}
