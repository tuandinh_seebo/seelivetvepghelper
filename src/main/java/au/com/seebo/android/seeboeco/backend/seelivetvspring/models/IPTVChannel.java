package au.com.seebo.android.seeboeco.backend.seelivetvspring.models;

import java.util.List;

/**
 * Created by root on 6/8/15.
 */
public class IPTVChannel {

    private String name;
    private String tvgName;
    private String logoName;
    private String streamingUrl;
    private List<Programme> epgData;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTvgName() {
        return tvgName;
    }

    public void setTvgName(String tvgName) {
        this.tvgName = tvgName;
    }

    public String getLogoName() {
        return logoName;
    }

    public void setLogoName(String logoName) {
        this.logoName = logoName;
    }

    public String getStreamingUrl() {
        return streamingUrl;
    }

    public void setStreamingUrl(String streamingUrl) {
        this.streamingUrl = streamingUrl;
    }

    public List<Programme> getEpgData() {
        return epgData;
    }

    public void setEpgData(List<Programme> epgData) {
        this.epgData = epgData;
    }

    public String toString(){

        return "Channel Name : " + getName() + " Channel TVG Name : " + getTvgName() + " Channel Logo : " + getLogoName() + " Channel Url : " + getStreamingUrl();

    }

}
