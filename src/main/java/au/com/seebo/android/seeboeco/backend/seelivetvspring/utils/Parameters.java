package au.com.seebo.android.seeboeco.backend.seelivetvspring.utils;

public class Parameters {

	public final String DEFAULT_M3U="http://iptv.seebo.com.au/IPTV/playlist-eco.m3u";
	public final String DEFAULT_EPG="http://iptv.seebo.com.au/IPTV/epg.xml";
	public final String DEFAULT_LOGO="";
}
