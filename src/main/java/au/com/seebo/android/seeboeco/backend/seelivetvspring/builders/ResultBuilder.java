package au.com.seebo.android.seeboeco.backend.seelivetvspring.builders;

import au.com.seebo.android.seeboeco.backend.seelivetvspring.models.Result;

public class ResultBuilder {

    public static Result buildSuccessMsg(String msg,Object data){

        Result result = new Result();

        result.setType("SUCCESS");
        result.setMsg(msg);

        if (data != null)
            result.setData(data);

        return result;

    }

    public static Result buildErrorMsg(String msg,Object data,int errCode){

        Result result = new Result();

        result.setType("ERROR");
        result.setMsg(msg);
        result.setCode(errCode);

        if (data != null)
            result.setData(data);

        return result;

    }

}