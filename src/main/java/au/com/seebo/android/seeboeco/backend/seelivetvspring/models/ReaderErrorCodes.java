package au.com.seebo.android.seeboeco.backend.seelivetvspring.models;

/**
 * Created by root on 6/18/15.
 */
public class ReaderErrorCodes {

    public static final int GENERAL_HTTP_ERROR = 7777;
    public static final int FILE_NOT_FOUND = 7788;
    public static final int IP_TV_EPG_PARSE_ERROR = 7799;

}
