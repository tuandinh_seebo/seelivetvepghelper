package au.com.seebo.android.seeboeco.backend.seelivetvspring.dto;

import java.util.List;

import au.com.seebo.android.seeboeco.backend.seelivetvspring.models.IPTVChannel;

public class MobileDto {

	private List<IPTVChannel> seeboTvChannels;
	private List<IPTVChannel> greekTvChannels;
	private List<IPTVChannel> ecoTvChannels;
	private String seeboTvErrorInfo;
	private String greekTvErrorInfo;
	private String ecoTvErrorInfo;	
	
	public List<IPTVChannel> getSeeboTvChannels() {
		return seeboTvChannels;
	}
	public void setSeeboTvChannels(List<IPTVChannel> seeboTvChannels) {
		this.seeboTvChannels = seeboTvChannels;
	}
	public List<IPTVChannel> getGreekTvChannels() {
		return greekTvChannels;
	}
	public void setGreekTvChannels(List<IPTVChannel> greekTvChannels) {
		this.greekTvChannels = greekTvChannels;
	}
	public List<IPTVChannel> getEcoTvChannels() {
		return ecoTvChannels;
	}
	public void setEcoTvChannels(List<IPTVChannel> ecoTvChannels) {
		this.ecoTvChannels = ecoTvChannels;
	}
	public String getSeeboTvErrorInfo() {
		return seeboTvErrorInfo;
	}
	public void setSeeboTvErrorInfo(String seeboTvErrorInfo) {
		this.seeboTvErrorInfo = seeboTvErrorInfo;
	}
	public String getGreekTvErrorInfo() {
		return greekTvErrorInfo;
	}
	public void setGreekTvErrorInfo(String greekTvErrorInfo) {
		this.greekTvErrorInfo = greekTvErrorInfo;
	}
	public String getEcoTvErrorInfo() {
		return ecoTvErrorInfo;
	}
	public void setEcoTvErrorInfo(String ecoTvErrorInfo) {
		this.ecoTvErrorInfo = ecoTvErrorInfo;
	}
	
}
