package au.com.seebo.android.seeboeco.backend.seelivetvspring.models;

/**
 * Created by root on 6/18/15.
 */
public class ErrorInfo {

    private String errMSG;
    private int errCode;

    public String getErrMSG() {
        return errMSG;
    }

    public void setErrMSG(String errMSG) {
        this.errMSG = errMSG;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }
}
