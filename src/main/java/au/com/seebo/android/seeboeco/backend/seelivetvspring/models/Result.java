package au.com.seebo.android.seeboeco.backend.seelivetvspring.models;

/**
 * Created by root on 6/5/15.
 */
public class Result {

    private String type;
    private String msg;
    private Object data;
    private int code;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
