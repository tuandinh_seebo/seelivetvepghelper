package au.com.seebo.android.seeboeco.backend.seelivetvspring.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by root on 6/10/15.
 */
public class ProgrammeUtils {

    public static String getTimezone(String time){

        Pattern timePattern = Pattern.compile("(\\+|-)(\\d+)");
        Matcher matcher = timePattern.matcher(time);

        if (matcher.find()){
            return matcher.group(1) + matcher.group(2);
        }

        return null;

    }

    public static String getDateTime(String time){

        Pattern timePattern = Pattern.compile("^(\\d{14})");
        Matcher matcher = timePattern.matcher(time);

        if (matcher.find()){
            return matcher.group(1);
        }

        return null;

    }

    public static String getCurrentTime(){

        Calendar cal = new GregorianCalendar(TimeZone.getDefault());

        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DATE);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        int sec = cal.get(Calendar.SECOND);
        int amPm = cal.get(Calendar.AM_PM);

        return cal.get(Calendar.YEAR) + "" + ((month < 10) ? "0" + month : month) + "" + ((day < 10) ? "0" + day : day) + "" +  ((hour < 10) ? "0" + hour : hour) + "" + ((min < 10) ? "0" + min : min) + "" + ((sec < 10) ? "0" + sec : sec);

    }

    public static String formatTime(String time,int format){

        String extractedDate = getDateTime(time);
        String formattedDate = null;

        int year = Integer.parseInt(extractedDate.substring(0,4));
        int month = Integer.parseInt(extractedDate.substring(4,6));
        int day = Integer.parseInt(extractedDate.substring(6,8));
        int hour = Integer.parseInt(extractedDate.substring(8,10));
        int min = Integer.parseInt(extractedDate.substring(10,12));
        int sec = Integer.parseInt(extractedDate.substring(12,14));
        int amPm = (hour < 12) ? 0 : 1;

        System.out.println(hour + " - " + min);

        switch (format){

            case DateFormats.MATCH_FORMAT:
                formattedDate = year + "" + ((month < 10) ? "0" + month : month) + "" + ((day < 10) ? "0" + day : day) + "" +  ((hour < 10) ? "0" + hour : hour) + "" + ((min < 10) ? "0" + min : min) + "" + ((sec < 10) ? "0" + sec : sec);
                break;
            case DateFormats.VIEWER_FORMAT:
                formattedDate = ((hour < 10) ? "0" + hour : hour) + "." + ((min < 10) ? "0" + min : min) + " " + ((amPm == 0) ? "AM" : "PM");
                break;
            default:
                formattedDate = year + "" + ((month < 10) ? "0" + month : month) + "" + ((day < 10) ? "0" + day : day) + "" +  ((hour < 10) ? "0" + hour : hour) + "" + ((min < 10) ? "0" + min : min) + "" + ((sec < 10) ? "0" + sec : sec);
                break;

        }

        return formattedDate;
    }

    public static boolean validateTime(String time){

        Pattern timePattern = Pattern.compile("^(\\d{14})(\\s+)(\\+|-)(\\d+)");
        Matcher matcher = timePattern.matcher(time);

        if (matcher.find() && matcher.groupCount() == 4){
            return true;
        }

        return false;

    }

    public static String formatTimeStamp(long time,int format){

        String formattedDate = null;

        switch (format){

            case DateFormats.VIEWER_FORMAT:
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(time * 1000);
                SimpleDateFormat sdf = new SimpleDateFormat("HH.mm a");
                formattedDate = sdf.format(calendar.getTime());
                break;
            default:
                SimpleDateFormat defaultDateFormat1 = new SimpleDateFormat("HH.mm.ss a");
                formattedDate = defaultDateFormat1.format(time);
                break;

        }

        return formattedDate;
    }

}
