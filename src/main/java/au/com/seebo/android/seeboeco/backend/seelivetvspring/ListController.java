package au.com.seebo.android.seeboeco.backend.seelivetvspring;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class ListController extends HttpServlet {
	
	public ListController() {}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		PrintWriter out = resp.getWriter();
		out.write("<html><body>");
		String category = req.getParameter("category");
		out.write("You chose " + category);
		out.write("</body></html>");
	}
	
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		resp.getWriter().write("<html><body> doGet </body></html>");
	}

}
