SeeliveTV EPG Helper
=============================

An internal engine to process IPTV channel and EPG data for Seebo mobile applications.

NOTE:

1. To build this app, you need maven 3.1 or above. 

2. Please refer to pom.xml for required dependencies 

3. To build: . /build.sh

4. To run as devserver: . /devserver.sh

5. Run local test: localhost:8080/iptv/default. Should show Channels info/EPGs (in unprocessed form but
designed to be handled by the Seebo mobile application).